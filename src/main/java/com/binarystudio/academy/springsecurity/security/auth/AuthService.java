package com.binarystudio.academy.springsecurity.security.auth;

import com.binarystudio.academy.springsecurity.domain.user.UserService;
import com.binarystudio.academy.springsecurity.domain.user.model.User;
import com.binarystudio.academy.springsecurity.security.auth.model.*;
import com.binarystudio.academy.springsecurity.security.jwt.JwtProvider;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service
public class AuthService {
    private final UserService userService;
    private final JwtProvider jwtProvider;
    private final PasswordEncoder passwordEncoder;

    public AuthService(UserService userService, JwtProvider jwtProvider, PasswordEncoder passwordEncoder) {
        this.userService = userService;
        this.jwtProvider = jwtProvider;
        this.passwordEncoder = passwordEncoder;
    }

    public AuthResponse performLogin(AuthorizationRequest authorizationRequest) {
        var userDetails = userService.loadUserByUsername(authorizationRequest.getUsername());
        if (passwordsDontMatch(authorizationRequest.getPassword(), userDetails.getPassword())) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Invalid password");
        }
        return jwtProvider.generateTokens(userDetails);
    }

    public AuthResponse performRegistration(RegistrationRequest registrationRequest) {
        var user = userService.register(registrationRequest);
        return jwtProvider.generateTokens(user);
    }

    private boolean passwordsDontMatch(String rawPw, String encodedPw) {
        return !passwordEncoder.matches(rawPw, encodedPw);
    }

    public AuthResponse refreshTokens(RefreshTokenRequest refreshTokenRequest) {
        var username = jwtProvider.getLoginFromToken(refreshTokenRequest.getRefreshToken());
        var user = userService.loadUserByUsername(username);
        return jwtProvider.generateTokens(user);
    }

    public String forgotPassword(String email) {
        var user = userService.loadUserByEmail(email);
        return jwtProvider.generateResetToken(user);
    }

    public AuthResponse replacePassword(ForgottenPasswordReplacementRequest forgottenPasswordReplacementRequest) {
        var username = jwtProvider.getLoginFromToken(forgottenPasswordReplacementRequest.getToken());
        return updatePassword(userService.loadUserByUsername(username), forgottenPasswordReplacementRequest.getNewPassword());
    }

    public AuthResponse updatePassword(User user, String password) {
        user.setPassword(passwordEncoder.encode(password));
        return jwtProvider.generateTokens(user);

    }
}
