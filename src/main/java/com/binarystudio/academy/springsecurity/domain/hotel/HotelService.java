package com.binarystudio.academy.springsecurity.domain.hotel;

import com.binarystudio.academy.springsecurity.domain.hotel.model.Hotel;
import com.binarystudio.academy.springsecurity.domain.user.model.User;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;

@Service
public class HotelService {
    private static final String ACCESS_MSG = "You are not owner of this hotel";

    private final HotelRepository hotelRepository;

    public HotelService(HotelRepository hotelRepository) {
        this.hotelRepository = hotelRepository;
    }

    public void delete(User user, UUID hotelId) {
        var hotel = getById(hotelId);
        checkOwner(user, hotel);
        hotelRepository.delete(hotelId);
    }

    public List<Hotel> getAll() {
        return hotelRepository.getHotels();
    }


    public Hotel update(User user, Hotel hotel) {
        var realHotel = getById(hotel.getId());
        checkOwner(user, realHotel);
        return hotelRepository.save(hotel);
    }

    public Hotel create(User user, Hotel hotel) {
        hotel.setOwnerId(user.getId());
        return hotelRepository.save(hotel);
    }

    public Hotel getById(UUID hotelId) {
        return hotelRepository.getById(hotelId).orElseThrow(NoSuchElementException::new);
    }

    private void checkOwner(User user, Hotel hotel) {
        if (!user.getId().equals(hotel.getOwnerId())) throw new AccessDeniedException(ACCESS_MSG);
    }
}
